package `3`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/29
* TITLE: Suma de fraccions
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    val number=scanner.nextInt()
    var result=1.0

    for (i in 2..number){
        result+=1.0/i.toDouble()
    }
    print(result)
}