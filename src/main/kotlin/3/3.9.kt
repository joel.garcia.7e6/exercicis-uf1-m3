package `3`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/28
* TITLE: És primer?
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    val number=scanner.nextInt()
    var checker=false

    for(i in 2 until number){
        if(number%i==0)checker=true
    }
    if (checker==true) print("No es primer")
    else print("Es primer")
}