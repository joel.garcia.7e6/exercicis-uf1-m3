package `3`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/6
* TITLE: Taula de multiplicar
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val num=scanner.nextInt()
    var resultat=0
    for (it in 1..10) {
        resultat=num*it
        println ("$num x $it = $resultat")
    }

}