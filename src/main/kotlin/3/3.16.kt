package `3`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/19
* TITLE: Coordenades en moviment
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    var x=0
    var y=0
    do{
        val num=scanner.next().single()
        if (num=='n') y--
        else if (num=='s') y++
        else if (num=='e') x++
        else if (num=='o') x--
    } while (num!='z')
    println("($x,$y)")
}