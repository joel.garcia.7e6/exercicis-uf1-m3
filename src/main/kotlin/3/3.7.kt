package `3`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/28
* TITLE: Revers de l’enter
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    val number=scanner.nextInt()
    var reversNum= number
    while (reversNum%10!=0){
        print(reversNum%10)
        reversNum/=10
    }
}