package `3`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/29
* TITLE: Divisible per 3 i per 5
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    val number=scanner.nextInt()

    for(i in 1..number){
        if(i%3==0 && i%5==0) println("$i divisible per 3 y per 5")
        else if (i%5==0) println("$i divisible per 5")
        else if (i%3==0) println("$i divisible per 3")
    }

}