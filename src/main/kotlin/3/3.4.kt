package `3`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/6
* TITLE: Imprimeix el rang(2)
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)

    val rang1=scanner.nextInt()
    val rang2=scanner.nextInt()
    for (a in rang1..rang2) println(a)
    for (b in rang1 downTo rang2) println(b)
}
