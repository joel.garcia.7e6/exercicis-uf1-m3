package `3`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/19
* TITLE: Factorial!
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val num=scanner.nextInt()
    var factorial=1
    for (i in 1..num){
        factorial*=i
    }
    println(factorial)
}