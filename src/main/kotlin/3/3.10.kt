package `3`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/28
* TITLE: És primer?
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    var number=scanner.nextInt()
    var min=number
    var max=number

    while (number!=0) {
        number=scanner.nextInt()
        if (number<min) min=number
        if (number>max) max=number
    }
    print("$max $min")

}