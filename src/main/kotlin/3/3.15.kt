package `3`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/19
* TITLE: Endevina el número (ara amb intents)
*/
import java.util.*
fun rand(start: Int, end: Int): Int {
    require(start <= end) { "Illegal Argument" }
    return (start..end).random()
}
fun main() {
    val scanner = Scanner(System.`in`)
    var i=0
    val start=1
    val end=100
    val random=rand(start, end)
    do{
        val num=scanner.nextInt()
        if (num > random) println("Massa alt")
        else if (num < random) println("Massa baix")
        i++
    } while (i<=6&&num!=random)



}