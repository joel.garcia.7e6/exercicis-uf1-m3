package `3`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/29
* TITLE: Triangle invertit d’*
*/
import java.lang.Math.abs
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    val n=scanner.nextInt()
    var asteriscos=n
    var espacios=0

    for (i in 1 .. n){


        repeat(espacios){print(" ")}
        repeat(asteriscos) {print("*")}
        println()
        espacios=i
        asteriscos--
    }
}