package `3`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/6
* TITLE: Calcula la suma dels N primers
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val num=scanner.nextInt()
    var resultat=0
    for (it in 1..num) {
        resultat+=it
    }
    println(resultat)
}