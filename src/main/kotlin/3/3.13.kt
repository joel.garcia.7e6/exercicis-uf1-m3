package `3`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/29
* TITLE: Logaritme natural de 2
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    val number=scanner.nextInt()
    var result=1.0

    for (i in 2..number){
        if(i%2==0)result-=1.0/i.toDouble()
        else result+=1.0/i.toDouble()

    }
    print(result)
}