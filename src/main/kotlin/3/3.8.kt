package `3`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/28
* TITLE: Nombre de dígits
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    var number=scanner.nextInt()
    var couter=0
    val sameNumber=number
    while (number%10!=0){
        number/=10
        couter++
    }
    print("El número $sameNumber té $couter dígits")
}