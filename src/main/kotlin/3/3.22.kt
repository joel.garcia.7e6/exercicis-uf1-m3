package `3`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/3
* TITLE: Rombe d'*
*/
import java.lang.Math.abs
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    val n=scanner.nextInt()
    for (i in 1 until 2*n){
        val espais= abs(n-i)
        val asteriscos= if (2*i-1<2*n) 2*i-1
                            else (2*i-1)-(abs(n-i)*4)
        repeat(espais){print(" ")}
        repeat(asteriscos){print("*")}
        println()
    }
}