package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/6
* TITLE: Calcula la lletra del dni
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    print ("Intodueix el numero del DNI")
    val DNI=scanner.nextInt()
    val x=DNI%23
    when (x){
        0 ->println ("$DNI T")
        1 ->println ("$DNI R")
        2 ->println ("$DNI W")
        3 ->println ("$DNI A")
        4 ->println ("$DNI G")
        5 ->println ("$DNI M")
        6 ->println ("$DNI Y")
        7 ->println ("$DNI F")
        8 ->println ("$DNI P")
        9 ->println ("$DNI D")
        10 ->println ("$DNI X")
        11 ->println ("$DNI B")
        12 ->println ("$DNI N")
        13 ->println ("$DNI J")
        14 ->println ("$DNI Z")
        15 ->println ("$DNI S")
        16 ->println ("$DNI Q")
        17 ->println ("$DNI V")
        18 ->println ("$DNI H")
        19 ->println ("$DNI L")
        20 ->println ("$DNI C")
        21 ->println ("$DNI K")
        22 ->println ("$DNI E")


    }
}