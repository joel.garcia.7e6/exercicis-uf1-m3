package `2`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/7
* TITLE: Quina nota he tret?
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val num= scanner.nextInt()
    print ("Introdueix la unitat de mesura")
    val unitat=scanner.next()
    if (unitat=="G"||unitat=="g"){
        print (num*0.000001)
        println ("Tn")
        print (num*0.001)
        println ("Kg")
    }
    else if (unitat== "TN" ||unitat== "tn"){
        print (num*1000.0)
        println ("Kg")
        print (num*1000000.0)
        println ("g")
    }
    else if (unitat== "KG" ||unitat== "kg"){
        print (num* 0.001)
        println ("Tn")
        print (num*1000.0)
        println ("g")
    }
}