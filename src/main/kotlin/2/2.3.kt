package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/29
* TITLE: És un bitllet vàlid
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    print ("introdueix un billet: ")
    val billet = scanner.nextInt()
    if (billet!=5&&billet!=10&&billet!=20&&billet!=50&&billet!=100&&billet!=200&&billet!=500){
        print (false)
    }
    else print (true)

}
