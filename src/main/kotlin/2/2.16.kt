package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/7
* TITLE: Quants dies té el mes
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    print ("Introdueix el mes: ")
    val mes=scanner.nextInt()
    if (mes in 0..12&&mes==2){
        println("28")
    }
    else if (mes in 0..12&&mes==4||mes==6||mes==9||mes==11){
        println("30")
    }
    else if (mes in 0..12&&mes==1||mes==3||mes==5||mes==7||mes==8||mes==10||mes==12){
        println("31")
    }
    else println ("Mes inexistent")

}