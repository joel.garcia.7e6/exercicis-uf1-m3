package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/29
* TITLE: Té edat per treballar
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    print ("Introdueix l'edat:")
    val edat = scanner.nextInt()
    if (edat>=16&&edat<=65) println ("Té edat per a treballat")
    else println ("No té edat per a treballar")

}