package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/7
* TITLE: Valor absolut
*/

import java.lang.Math.abs
import java.util.*
fun main(args : Array<String>) {
    val scanner = Scanner(System.`in`)
    print("Introdueix un numero: ")
    val num1 = scanner.nextInt()
    print (abs(num1))
}