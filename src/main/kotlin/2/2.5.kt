package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/29
* TITLE: En rang
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    print ("Introdueix els limits del rang: ")
    val a = scanner.nextInt()
    val b = scanner.nextInt()
    print ("Introdueix el valors dins del rang anterior: ")
    val c = scanner.nextInt()
    val d = scanner.nextInt()
    val e = scanner.nextInt()
    when{
        c in a..b -> println (true)
        c in b..a -> println (true)
        d in a..b -> println (true)
        d in b..a -> println (true)
        e in a..b -> println (true)
        e in b..a -> println (true)
        else -> println (false)
    }
}