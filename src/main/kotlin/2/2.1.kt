package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/29
* TITLE: Màxim de 3 nombres enters
*/

import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    print("Introdueix un numero a: ")
    val a = scanner.nextInt()
    print("Introdueix un numero b: ")
    val b = scanner.nextInt()
    print("Introdueix un numero c: ")
    val c = scanner.nextInt()
    if (a>=b&&a>=c) println (a)
    else if (b>=a&&b>=c) println (b)
    else if (c>=a&&c>=b) println (c)

}