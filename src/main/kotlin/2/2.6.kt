package `2`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/6
* TITLE: Quina pizza és més gran?
*/

import java.util.*
fun main(){
    val scanner=Scanner(System.`in`)
    print ("Introdueix el diametre de la pizza rodona:")
    val diametre=scanner.nextInt()
    print ("Introdueix la llargada de la pizza rectangular:")
    val llargada=scanner.nextInt()
    print ("Introdueix l'amplada de la pizza rectangular:")
    val amplada=scanner.nextInt()
    val pizza1= ((diametre/2)*(diametre/2)*Math.PI)
    val pizza2= amplada*llargada
    if (pizza1>pizza2) println ("Pizza rodona $pizza1")
    else println ("pizza rectangular $pizza2")

}