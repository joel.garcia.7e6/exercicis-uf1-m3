package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/7
* TITLE: Quina nota he tret?
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    print("Introdueix la teva nota: ")
    val nota= scanner.nextDouble()
    when (nota){
        in 0.0..4.9 -> println("Insuficient")
        in 5.0..5.9 -> println("Suficient")
        in 6.0..6.9 -> println("Bé")
        in 7.0..8.9 -> println("Notable")
        in 9.0..10.0 -> println("Excel·lent")
        else -> println("Nota incorrecta")
}
}