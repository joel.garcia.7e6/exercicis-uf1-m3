package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/6
* TITLE: És vocal o consonant?
*/


import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val letra=scanner.next().single()
    when (letra){
        'a','e','i','o','u','A','E','I','O','U' -> print ("es vocal")
        else -> print("es consonant")
    }
}