package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/7
* TITLE: Puges o baixes?
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    print ("Introdueix un numero: ")
    val num1=scanner.nextInt()
    print ("Introdueix un altre numero: ")
    val num2=scanner.nextInt()
    print ("Introdueix un tercer numero: ")
    val num3=scanner.nextInt()
    if (num1<num2&&num2<num3){
        print ("puges")
    }
    else if (num1>num2&&num2>num3){
        print ("baixes")
    }
    else print("cap de les dues")
}