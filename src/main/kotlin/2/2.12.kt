package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/6
* TITLE: Comprova la data
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    print ("Introdueix el dia: ")
    val dia=scanner.nextInt()
    print ("Introdueix el mes: ")
    val mes=scanner.nextInt()
    print ("Introdueix l'any: ")
    val año=scanner.nextInt()
    if (año in 0..2022&&dia in 0..28&&mes in 0..12&&mes==2){
        println("La data es correcta")
    }
    else if (año in 0..2022&&dia in 0..30&&mes in 0..12&&mes==4||mes==6||mes==9||mes==11){
        println("La data es correcta")
    }
    else if (año in 0..2022&&dia in 0..31&&mes in 0..12&&mes==1||mes==3||mes==5||mes==7||mes==8||mes==10||mes==12){
        println("La data es correcta")
    }
    else println ("La data no es correcta")
}
