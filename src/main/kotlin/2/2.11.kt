package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/6
* TITLE: Calculadora
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    print ("Introdueix un numero:")
    val x= scanner.nextInt()
    print ("Introdueix un altre numero:")
    val y=scanner.nextInt()
    print ("Introdueix la operació:")
    val operacion=scanner.next().single()
    when (operacion){
        '+' -> println(x+y)
        '-'-> println(x-y)
        '*' -> println(x*y)
        '/' -> println(x/y)
        '%' -> println(x%y)
    }
}