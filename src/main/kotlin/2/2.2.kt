package `2`/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/29
* TITLE: Salari
*/

import java.util.*
fun main (){
    val scanner = Scanner(System.`in`)
    print ("Introdueix les hores treballades: ")
    val hores = scanner.nextInt()
    if (hores>40) println (hores*40+((hores-40)*((50 * 40) / 100)))
    else println (hores*40)
}
