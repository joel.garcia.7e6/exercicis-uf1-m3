package `1`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: És divisible
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val num1 = scanner.nextInt()
    val num2 = scanner.nextInt()
    if (num1%num2==0||num2%num1==0) print(true)
    else print (false)
}