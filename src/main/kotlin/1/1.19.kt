/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: Programa adolescent
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un booleà:")
    val a = scanner.nextBoolean()
    print(!a)

}