/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: Calcula el descompte
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el preu original")
    val a = scanner.nextDouble()
    println("Introdueix el preu actual")
    val b = scanner.nextDouble()
    print ((a-b)/a*100)
    print ("%")

}
