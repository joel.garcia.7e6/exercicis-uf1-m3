/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/19
* TITLE: Dobla l’enter
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt()
    println(userInputValue*2)
}
