/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: Número següent
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val a = scanner.nextInt()
    print("Després ve el ")
    print (a+1)

}
