/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: Dobla el decimal
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val a = scanner.nextDouble()
    print (a*2)

}
