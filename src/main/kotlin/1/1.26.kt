/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/22
* TITLE: Fes-me minúscula
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un una lletra:")
    val userInputValue = scanner.next().single()
    print (userInputValue.toLowerCase())


}