package `1`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: Ajuda per la màquina de viatge en el temps
*/
import java.time.LocalDate
fun main() {
    print(LocalDate.now())
}