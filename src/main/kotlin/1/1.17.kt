/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: És edat legal
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix l'edat")
    val a = scanner.nextInt()
    print (a>=18)

}
