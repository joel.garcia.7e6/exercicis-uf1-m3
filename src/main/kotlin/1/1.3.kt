/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/19
* TITLE: Suma de dos nombres enters
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue1 = scanner.nextInt()
    println("Introdueix un altre número:")
    val userInputValue2 = scanner.nextInt()
    println(userInputValue1+userInputValue2)
}
