/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: Transforma l’enter
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número")
    val a = scanner.nextInt()
    print (a*1.0)
}
