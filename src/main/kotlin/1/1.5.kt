/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/19
* TITLE: Operació boja
*/

import java.util.*
fun main() {
    println("per a calcular (a+b)*(c%d)")
    val scanner = Scanner(System.`in`)
    println("Introdueix a:")
    val a = scanner.nextInt()
    println("Introdueix b:")
    val b = scanner.nextInt()
    println("Introdueix c:")
    val c = scanner.nextInt()
    println("Introdueix d:")
    val d = scanner.nextInt()
    println((a+b)*(c%d))
}
