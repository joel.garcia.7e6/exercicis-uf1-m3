/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: De Celsius a Fahrenheit
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix la temperatura en Cº")
    val t = scanner.nextDouble()
    print ((t*9/5)+32)
}
