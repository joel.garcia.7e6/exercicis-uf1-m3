/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/19
* TITLE: Calcula l’àrea
*/

import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Introdueix l'amplada:")
    val scanner2 = Scanner(System.`in`)
    println("Introdueix la llargada")
    val userInputValue1 = scanner1.nextInt()
    val userInputValue2 = scanner2.nextInt()
    println(userInputValue1*userInputValue2)
}
