/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/09/21
* TITLE: Afegeix un segon
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix el segon")
    val s = scanner.nextInt()

    print ((s+1)%60)
}
