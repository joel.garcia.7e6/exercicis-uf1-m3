package `1`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/21
* TITLE: Quant de temps?
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val num=scanner.nextInt()
    val h=num/60
    val hours=num%60+1
    val m=num-h
    val min=h%60
    val s=num-m
    val sec=m%60
    print("$hours hores $min minuts $sec segons ")
}