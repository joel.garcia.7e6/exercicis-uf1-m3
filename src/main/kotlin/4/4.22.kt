package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/17
* TITLE: Palíndrom
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    val entrada=scanner.next()
    val palindrom=entrada.split("")

    var a=0
    var start= String()
    var end= String()

    val ascentList= mutableListOf("null")
    ascentList.clear()
    val descentList= mutableListOf("null")
    descentList.clear()
    for (i in 0..palindrom.lastIndex){
        start=palindrom[i]
        ascentList.add(i,start)
    }

    for (i in palindrom.lastIndex downTo 0){
        end=palindrom[i]
        descentList.add(a,end)
        a++
    }

    if(ascentList==descentList) println("És un palíndrom")
    else println("No és un palíndrom")

}