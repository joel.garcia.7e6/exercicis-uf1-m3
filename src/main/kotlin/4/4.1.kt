package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/19
* TITLE: Suma els valors
*/
fun main(args: Array<String>) {
    var resulado=0
    for (arg in args){
        var suma=arg.toInt()
        resulado+=suma
    }
    print (resulado)
}