package `4`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/29
* TITLE: És contingut?
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num=scanner.nextInt()
    val array= arrayOf(1,2,3,4,5,6,7,8,9,10)
    if (num in array) print("Si està contingut")
    else print("No està contingut")
}