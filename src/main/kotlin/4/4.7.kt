package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/28
* TITLE: Inverteix l’array
*/
fun main (args: Array<String>){
    val invertedArray=Array(args.size){"0"}
    for (i in 0..args.lastIndex){
        invertedArray[i]=args[args.lastIndex-i]

    }
    for (elem in invertedArray) print ("$elem ")
}