package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/28
* TITLE: Són iguals?(2)
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    val string1= scanner.next()
    val string2= scanner.next()
    if (string1.uppercase()==string2.uppercase()) print("si")
    else print("no")
}