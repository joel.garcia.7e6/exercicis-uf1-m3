package `4`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/19
* TITLE: Igual a l'últim
*/
fun main(args: Array<String>) {
    val last=args.last()
    var total=0
    for (i in 0 until args.lastIndex) {
        if (args[i] == last){
            total++
        }
    }
    println(total)
}



