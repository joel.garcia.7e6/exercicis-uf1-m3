package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/28
* TITLE: Distància d'Hamming
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    val ADN1= scanner.next()
    val ADN2= scanner.next()
    val hamming1=ADN1.split("")
    val hamming2=ADN2.split("")

    var contador=0

    if(hamming1.size!=hamming2.size)println("Entrada no vàlida")
    else{
        for(i in 0..hamming1.lastIndex){
            if(hamming1[i]!=hamming2[i]) contador++
        }
    }

    print(contador)
}