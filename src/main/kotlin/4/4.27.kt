package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/18
* TITLE: Elimina les repeticions
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val n= scanner.nextInt()

    val numberList= mutableListOf<Int>()
    val numberListAux= mutableListOf<Int>()

    for(i in 1 ..n){
        numberList.add(scanner.nextInt())
    }

    for (i in numberList.indices){
        if (numberList[i] !in numberListAux) numberListAux.add(numberList[i])
    }
    print(numberListAux)
}


