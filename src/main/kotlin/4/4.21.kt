package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/17
* TITLE: Parèntesis
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    val entrada=scanner.next()

    var abrir=0
    var cerrar=0

    for (i in entrada){
        if(i=='(') abrir++
        else if (i==')') cerrar++
        if (cerrar>abrir)break
    }
    if (abrir==cerrar) println("Si")
    else println("No")
}