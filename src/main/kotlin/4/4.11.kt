package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/28
* TITLE: Quants parells i quants senars?
*/
import java.util.*
fun main(args: Array<String>) {
    var pairCount=0
    var oddCount=0
    for(i in args){
        if (i.toInt()%2==0){
            pairCount++
        }
        else oddCount++
    }
    println("Parells: $pairCount")
    println("Senars: $oddCount")
}