package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/18
* TITLE: Quants sumen...?
*/
import java.util.*
fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    for (i in args) {
        for (j in args) {
            if (i.toInt()+j.toInt()==n) println("$i $j")
        }

    }
}