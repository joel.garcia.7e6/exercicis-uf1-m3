package `4`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/12/1
* TITLE: La suma total
*/

fun main(args: Array<String>) {

    val listOfNumbers = arrayOf<Int>(1,0,5,3,1,0)
    val list= mutableListOf<Int>()
    var suma=0
    for (num in listOfNumbers) {
        list.add(num)
        suma+=num
    }
    list.sort()
    if (suma-list.lastIndex==list.lastIndex)println("Si")
    else println("No")


}
