package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/19
* TITLE: Igual a l'últim
*/
import java.util.*
fun main(args: Array<String>) {
    var revisor=args[0].toInt()
    for(i in args){
        if (i.toInt() != revisor)break
        else revisor++
    }
    println (revisor)
}