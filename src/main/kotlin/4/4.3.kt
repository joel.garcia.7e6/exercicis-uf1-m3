package `4`

/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/29
* TITLE: Calcula la lletra del dni
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val DNI=scanner.nextInt()
    val letra= arrayOf('T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E')
    print("$DNI${letra[DNI%23]}")
}