package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/10/19
* TITLE: Suma els valors
*/
fun main(args: Array<String>) {
    var resultado=0.0
    for (arg in args){
        var suma=arg.toDouble()
        resultado+=suma
    }

    print (resultado/args.size)
}