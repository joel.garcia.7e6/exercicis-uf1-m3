package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/12/1
* TITLE: Ordena l’array (down)
*/

fun main(args: Array<String>) {

    val listOfNumbers = arrayOf<Int>(3,54,23,1,345,623,1,12,4)
    val list= mutableListOf<Int>()
    for (num in listOfNumbers) {
        list.add(num)
    }
    list.sortDescending()
    println(list)
}

