package `4`
/*
* AUTHOR: Joel Garcia Galiano
* DATE: 2022/11/17
* TITLE: 4.24 Inverteix les paraules (2)
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val entrada=scanner.nextLine()

    var word= String()

    val wordList=entrada.split(" ")

    for (i in wordList.lastIndex downTo 0){
        word=wordList[i]
        print(word+" ")
    }

    }
